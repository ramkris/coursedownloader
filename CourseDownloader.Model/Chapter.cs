﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseDownloader.Model
{
    public class Chapter
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public Dictionary<string, string> Tokens { get; set; }

        public bool IsDownloaded { get; set; }

        public string[] FileNameParts { get; set; }

        public int TotalMilliSeconds { get; set; }

        public Chapter()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
