﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CourseDownloader.Model
{
    public class Module
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public List<Chapter> Chapters { get; set; }

        public Dictionary<string, string> Tokens { get; set; }

        public bool IsDownloaded
        {
            get
            {
                return Chapters.Count == Chapters.Where(c => c.IsDownloaded).Count();
            }
        }

        public Module()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
