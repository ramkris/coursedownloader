﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CourseDownloader.Model
{
    public class Course
    {
        public string Id { get; private set; }

        public string Url { get; set; }

        public string Title { get; set; }

        public Dictionary<string, string> Tokens { get; set; }

        public List<Module> Modules { get; set; }

        public bool IsDownloaded
        {
            get
            {
                return TotalChaptersCount == DownloadedChaptersCount;
            }
        }

        public int TotalChaptersCount
        {
            get
            {
                return Modules.Sum(module => module.Chapters.Count);
            }
        }

        public int DownloadedChaptersCount
        {
            get
            {
                return Modules.Sum(module => module.Chapters.Where(c => c.IsDownloaded).Count());
            }
        }

        public Course()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
