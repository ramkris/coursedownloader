﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace CourseDownloader.Model
{
    public class WebClientEx : WebClient
    {
        private readonly CookieContainer container = new CookieContainer();

        public CookieContainer CookieContainer
        {
            get
            {
                return container;
            }
        }

        public bool RedirectFlag
        {
            get;
            set;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest r = base.GetWebRequest(address);
            var request = r as HttpWebRequest;

            request.MaximumAutomaticRedirections = 10;
            request.AllowAutoRedirect = RedirectFlag;
            request.CookieContainer = container;
            return r;
        }

        protected override WebResponse GetWebResponse(WebRequest request, IAsyncResult result)
        {
            WebResponse response = base.GetWebResponse(request, result);
            ReadCookies(response);
            return response;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            ReadCookies(response);
            return response;
        }

        private void ReadCookies(WebResponse r)
        {
            var response = r as HttpWebResponse;
            if (response != null)
            {
                CookieCollection cookies = response.Cookies;
                container.Add(cookies);
            }
        }
    }
}
