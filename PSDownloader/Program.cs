﻿using CourseDownloader.CourseParser;
using CourseDownloader.Model;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CourseDownloader.Utility;
using System.IO;

namespace PSDownloader
{
    static class Program
    {
        static Settings settings = Settings.Instance;
        public static ICourseParser parser = null;

        [STAThread]
        static void Main()
        {
            if (settings.Trainer.ToLower() == "pluralsight")
                parser = new PluralsightParser();

            if (settings.Trainer.ToLower() == "lynda")
                parser = new LyndaParser();

            Helper.CreateDataFolder();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            Application.ApplicationExit += Application_ApplicationExit;
            //Application.ThreadException += Application_ThreadException;
            //AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Application.Run(new MainForm());
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = (Exception)e.ExceptionObject;
            ShowError(ex);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            var ex = e.Exception;
            ShowError(ex);
        }

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            try
            {
                parser.Logout();
            }
            catch
            {
            }
        }

        static void ShowError(Exception ex)
        {
            var logMsg = string.Empty;
            logMsg += ex.Source;
            logMsg += Environment.NewLine;
            logMsg += Environment.NewLine;
            logMsg += ex.Message;
            logMsg += Environment.NewLine;
            logMsg += Environment.NewLine;
            logMsg += ex.StackTrace;

            var fileName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            fileName = Path.Combine(fileName, settings.SoftwareName + " ErrLog.txt");
            File.AppendAllText(fileName, logMsg);

            var displayMsg = string.Empty;
            displayMsg += "An error log is created on your desktop.";
            displayMsg += "Please send the log to our email at " + settings.SupportEmail + ".";
            displayMsg += "If you report the error to us, we'll give you 50% discount on the list price in case if you haven't bought the tool.";

            MessageBox.Show(displayMsg, "Error Occurred.", MessageBoxButtons.OK, MessageBoxIcon.Error);

            Application.Exit();
        }
    }
}
