﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CourseDownloader.CourseParser;
using CourseDownloader.Model;
using CourseDownloader.Utility;
using CourseDownloader.UI.Forms;
using System.Net;
using System.Threading;
using System.Diagnostics;

namespace PSDownloader
{
    public partial class MainForm : Form
    {
        Settings settings = Settings.Instance;
        Downloader downloader = Downloader.Instance;
        ICourseParser parser = Program.parser;

        public MainForm()
        {
            InitializeComponent();

            downloader.ChapterDownloadStarted += downloader_ChapterDownloadStarted;
            downloader.ChapterDownloadCompleted += downloader_ChapterDownloadCompleted;
            downloader.DownloadProgressChanged += downloader_DownloadProgressChanged;
            downloader.CourseDownloadPaused += downloader_CourseDownloadPaused;
            downloader.CourseDownloadCompleted += downloader_CourseDownloadCompleted;

            downloader.OnLicenseReset = OnLicenseResetHandler;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Text = settings.SoftwareName;
            cbSimulateRealUser.Checked = settings.SimulateRealUser;

            var tip = "Simulates the behavior of a real user. And as a result downloads slow down considerably.";
            tip += "Check this option to avoid getting blocked by {0}.";
            cbToolTip.SetToolTip(cbSimulateRealUser, string.Format(tip, settings.Trainer));

            if (!Helper.IsTrialVersion())
                btnBuy.Visible = false;

            //if (settings.Trainer.ToLower() != "pluralsight")
            //    btnLoadCookie.Visible = false;

            btnLoadCookie.Visible = true;

            if (!Helper.IsUserConfigSetup())
                ShowSettingsForm();
            else
                Login();

            courseListControl1.PopulateCourseList();
            courseListControl1.OnCourseRemoved = OnCourseRemovedHandler;

            downloader.Parser = parser;
        }

        private void OnCourseRemovedHandler(Course course)
        {
            Debug.Assert(course != downloader.DownloadingCourse);
            moduleListControl1.Modules = null;
            Helper.SaveDataFile(courseListControl1.Courses);
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            AboutForm about = new AboutForm();
            about.ShowDialog();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            ShowSettingsForm();
        }

        private void ShowSettingsForm()
        {
            SettingsForm settingsForm = new SettingsForm();
            settingsForm.ShowDialog();

            if (!parser.IsLoggedIn)
                Login();
        }

        private void Login()
        {
            if (!Helper.IsUserConfigSetup())
            {
                MessageBox.Show("Can't login as settings are not complete.");
                lblStatus.Visible = true;
                lblStatus.Text = "Not Logged in.";
                return;
            }

            Cursor = Cursors.WaitCursor;
            lblStatus.Visible = true;
            lblStatus.Text = "Logging in";
            pbLoginProgress.Visible = true;
            mainToolbar.Enabled = false;

            bgwLogin.RunWorkerAsync();
        }

        private void btnAddCourse_Click(object sender, EventArgs e)
        {
            AddCourseForm addCourseForm = new AddCourseForm();
            addCourseForm.Parser = parser;

            addCourseForm.ShowDialog();

            var course = addCourseForm.Course;

            if (course != null)
            {
                courseListControl1.AddCourse(course);
                Helper.SaveDataFile(courseListControl1.Courses);
            }
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            BuyActivateForm buyForm = new BuyActivateForm();
            buyForm.ShowDialog();

            if (!Helper.IsTrialVersion())
                btnBuy.Visible = false;
        }

        private void bgwLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = parser.Login(settings.UserHandle, settings.Password);
        }

        private void bgwLogin_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pbLoginProgress.Visible = false;
            Cursor = Cursors.Arrow;
            mainToolbar.Enabled = true;

            if (e.Error != null)
            {
                lblStatus.Text = e.Error.Message;

                var msg = e.Error.Message;
                msg += Environment.NewLine;
                msg += Environment.NewLine;
                msg += "1. Retry if you are sure your user credentials are correct and you are connected to the Internet.";
                msg += Environment.NewLine;
                msg += "2. Cancel to go to settings dialog to correct your user credentials.";

                var dr = MessageBox.Show(msg, "", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                if (dr == DialogResult.Retry)
                    Login();
            }
            else if (e.Result is bool && (bool)e.Result)
            {
                lblStatus.Text = "Logged in.";
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Helper.SaveDataFile(courseListControl1.Courses);
        }

        private void btnDownloadCompleteCourse_Click(object sender, EventArgs e)
        {
            DownloadChapters(moduleListControl1.AllChapters);
        }

        private void btnDownloadSelectedChapters_Click(object sender, EventArgs e)
        {
            DownloadChapters(moduleListControl1.SelectedChapters);
        }

        private void DownloadChapters(List<Chapter> chapters)
        {
            var msg = string.Empty;

            if (!parser.IsLoggedIn)
            {
                msg += "You are not logged in.";
                msg += Environment.NewLine;
            }

            if (chapters == null || chapters.Count <= 0)
            {
                msg += "No chapter is selected.";
                msg += Environment.NewLine;
            }

            if (downloader.IsBusy)
            {
                msg += "Another download is in progress. Pause the current download to start a new download.";
                msg += Environment.NewLine;
                moduleListControl1.Modules = courseListControl1.SelectedCourse.Modules;
            }

            if (!string.IsNullOrEmpty(msg))
            {
                msg = "Fix the following..." + Environment.NewLine + Environment.NewLine + msg;
                MessageBox.Show(msg, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            lblStatus.Visible = false;
            downloader.DownloadChapters(chapters, courseListControl1.SelectedCourse);
        }

        void downloader_ChapterDownloadCompleted(Chapter chapter)
        {
            Helper.SaveDataFile(courseListControl1.Courses);
        }

        void downloader_CourseDownloadPaused(Course course)
        {
            Invoker((MethodInvoker)delegate
            {
                lblStatus.Visible = true;
                lblStatus.Text = "Download Paused";
                lblChapterName.Visible = false;
                lblProgress.Visible = false;
                pbDownloadProgress.Visible = false;
            });
        }

        void downloader_ChapterDownloadStarted(Chapter chapter)
        {
            Invoker((MethodInvoker)delegate
            {
                lblStatus.Visible = false;
                lblChapterName.Visible = true;
                lblChapterName.Text = chapter.Title;
                lblProgress.Text = string.Empty;
                lblProgress.Visible = true;
                pbDownloadProgress.Visible = true;
                pbDownloadProgress.Value = 0;
            });
        }

        private void downloader_DownloadProgressChanged(int progress)
        {
            Invoker((MethodInvoker)delegate
            {
                pbDownloadProgress.Value = progress;
                lblProgress.Text = progress.ToString() + "%";
            });
        }

        private void downloader_CourseDownloadCompleted(Course course)
        {
            Invoker((MethodInvoker)delegate
            {
                lblStatus.Visible = false;
                lblChapterName.Visible = false;
                lblProgress.Visible = false;
                pbDownloadProgress.Visible = false;

                MessageBox.Show("Download completed.");
            });
        }

        private void Invoker(MethodInvoker methodToInvoke)
        {
            if (this.Disposing || this.IsDisposed)
                return;

            try
            {
                this.Invoke(methodToInvoke);
            }
            catch { }
        }

        private void OnLicenseResetHandler(object obj)
        {
            Invoker((MethodInvoker)delegate
            {
                btnBuy.Visible = true;
            });
        }

        private void courseListControl1_CourseSelected(Course selectedCourse)
        {
            moduleListControl1.Modules = selectedCourse.Modules;
            Text = settings.SoftwareName + " - " + selectedCourse.Title;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            if (!downloader.IsBusy)
                return;

            var dr = MessageBox.Show("Are you sure you want to pause the current download?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
                downloader.PauseDownload();
        }

        private void cbSimulateRealUser_CheckedChanged(object sender, EventArgs e)
        {
            settings.SimulateRealUser = cbSimulateRealUser.Checked;
        }

        private void btnLoadCookie_Click(object sender, EventArgs e)
        {
            LoadCookieForm cookieForm = new LoadCookieForm();
            cookieForm.Parser = parser;
            cookieForm.ShowDialog();
        }
    }
}
