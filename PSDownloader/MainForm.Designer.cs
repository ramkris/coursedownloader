﻿namespace PSDownloader
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainToolbar = new System.Windows.Forms.ToolStrip();
            this.btnAddCourse = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnDownloadCompleteCourse = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDownloadSelectedChapters = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPause = new System.Windows.Forms.ToolStripButton();
            this.btnSettings = new System.Windows.Forms.ToolStripButton();
            this.btnAbout = new System.Windows.Forms.ToolStripButton();
            this.btnBuy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLoadCookie = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbLoginProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.lblChapterName = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbDownloadProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.lblProgress = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.courseListControl1 = new CourseDownloader.UI.Controls.CourseListControl();
            this.moduleListControl1 = new CourseDownloader.UI.Controls.ModuleListControl();
            this.bgwLogin = new System.ComponentModel.BackgroundWorker();
            this.cbSimulateRealUser = new System.Windows.Forms.CheckBox();
            this.cbToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainToolbar.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainToolbar
            // 
            this.mainToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddCourse,
            this.toolStripButton2,
            this.btnPause,
            this.btnSettings,
            this.btnAbout,
            this.btnBuy,
            this.toolStripSeparator1,
            this.btnLoadCookie});
            this.mainToolbar.Location = new System.Drawing.Point(0, 0);
            this.mainToolbar.Name = "mainToolbar";
            this.mainToolbar.Size = new System.Drawing.Size(624, 25);
            this.mainToolbar.TabIndex = 0;
            this.mainToolbar.Text = "toolStrip1";
            // 
            // btnAddCourse
            // 
            this.btnAddCourse.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddCourse.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCourse.Image")));
            this.btnAddCourse.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddCourse.Name = "btnAddCourse";
            this.btnAddCourse.Size = new System.Drawing.Size(23, 22);
            this.btnAddCourse.Text = "toolStripButton1";
            this.btnAddCourse.ToolTipText = "Add Course";
            this.btnAddCourse.Click += new System.EventHandler(this.btnAddCourse_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDownloadCompleteCourse,
            this.btnDownloadSelectedChapters});
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(29, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "Download Course";
            // 
            // btnDownloadCompleteCourse
            // 
            this.btnDownloadCompleteCourse.Name = "btnDownloadCompleteCourse";
            this.btnDownloadCompleteCourse.Size = new System.Drawing.Size(225, 22);
            this.btnDownloadCompleteCourse.Text = "Download Complete Course";
            this.btnDownloadCompleteCourse.Click += new System.EventHandler(this.btnDownloadCompleteCourse_Click);
            // 
            // btnDownloadSelectedChapters
            // 
            this.btnDownloadSelectedChapters.Name = "btnDownloadSelectedChapters";
            this.btnDownloadSelectedChapters.Size = new System.Drawing.Size(225, 22);
            this.btnDownloadSelectedChapters.Text = "Download Selected Chapters";
            this.btnDownloadSelectedChapters.Click += new System.EventHandler(this.btnDownloadSelectedChapters_Click);
            // 
            // btnPause
            // 
            this.btnPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPause.Image = ((System.Drawing.Image)(resources.GetObject("btnPause.Image")));
            this.btnPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(23, 22);
            this.btnPause.Text = "toolStripButton4";
            this.btnPause.ToolTipText = "Pause Download";
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(23, 22);
            this.btnSettings.Text = "toolStripButton5";
            this.btnSettings.ToolTipText = "Change Settings";
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnAbout.Image")));
            this.btnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(23, 22);
            this.btnAbout.Text = "About";
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnBuy
            // 
            this.btnBuy.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnBuy.Image = ((System.Drawing.Image)(resources.GetObject("btnBuy.Image")));
            this.btnBuy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(95, 22);
            this.btnBuy.Text = "Buy/Activate";
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnLoadCookie
            // 
            this.btnLoadCookie.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLoadCookie.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadCookie.Image")));
            this.btnLoadCookie.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLoadCookie.Name = "btnLoadCookie";
            this.btnLoadCookie.Size = new System.Drawing.Size(23, 22);
            this.btnLoadCookie.Text = "Load Cookie";
            this.btnLoadCookie.ToolTipText = "Load Cookie Manually. A fix for CAPTCHA problem.";
            this.btnLoadCookie.Click += new System.EventHandler(this.btnLoadCookie_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.pbLoginProgress,
            this.lblChapterName,
            this.pbDownloadProgress,
            this.lblProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 420);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(624, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(52, 17);
            this.lblStatus.Text = "lblStatus";
            this.lblStatus.Visible = false;
            // 
            // pbLoginProgress
            // 
            this.pbLoginProgress.Name = "pbLoginProgress";
            this.pbLoginProgress.Size = new System.Drawing.Size(100, 16);
            this.pbLoginProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbLoginProgress.Visible = false;
            // 
            // lblChapterName
            // 
            this.lblChapterName.Name = "lblChapterName";
            this.lblChapterName.Size = new System.Drawing.Size(94, 17);
            this.lblChapterName.Text = "lblChapterName";
            this.lblChapterName.Visible = false;
            // 
            // pbDownloadProgress
            // 
            this.pbDownloadProgress.Name = "pbDownloadProgress";
            this.pbDownloadProgress.Size = new System.Drawing.Size(100, 16);
            this.pbDownloadProgress.Visible = false;
            // 
            // lblProgress
            // 
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(65, 17);
            this.lblProgress.Text = "lblProgress";
            this.lblProgress.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.lblProgress.Visible = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.courseListControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.moduleListControl1);
            this.splitContainer1.Size = new System.Drawing.Size(624, 395);
            this.splitContainer1.SplitterDistance = 140;
            this.splitContainer1.TabIndex = 2;
            // 
            // courseListControl1
            // 
            this.courseListControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.courseListControl1.Location = new System.Drawing.Point(0, 0);
            this.courseListControl1.Name = "courseListControl1";
            this.courseListControl1.Size = new System.Drawing.Size(624, 140);
            this.courseListControl1.TabIndex = 0;
            this.courseListControl1.CourseSelected += new CourseDownloader.UI.Controls.CourseSelectedHandler(this.courseListControl1_CourseSelected);
            // 
            // moduleListControl1
            // 
            this.moduleListControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moduleListControl1.Location = new System.Drawing.Point(0, 0);
            this.moduleListControl1.Name = "moduleListControl1";
            this.moduleListControl1.Size = new System.Drawing.Size(624, 251);
            this.moduleListControl1.TabIndex = 0;
            // 
            // bgwLogin
            // 
            this.bgwLogin.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwLogin_DoWork);
            this.bgwLogin.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwLogin_RunWorkerCompleted);
            // 
            // cbSimulateRealUser
            // 
            this.cbSimulateRealUser.AutoSize = true;
            this.cbSimulateRealUser.BackColor = System.Drawing.SystemColors.Control;
            this.cbSimulateRealUser.Location = new System.Drawing.Point(267, 4);
            this.cbSimulateRealUser.Name = "cbSimulateRealUser";
            this.cbSimulateRealUser.Size = new System.Drawing.Size(116, 17);
            this.cbSimulateRealUser.TabIndex = 3;
            this.cbSimulateRealUser.Text = "Simulate Real User";
            this.cbSimulateRealUser.UseVisualStyleBackColor = false;
            this.cbSimulateRealUser.CheckedChanged += new System.EventHandler(this.cbSimulateRealUser_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 442);
            this.Controls.Add(this.cbSimulateRealUser);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.mainToolbar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainToolbar.ResumeLayout(false);
            this.mainToolbar.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mainToolbar;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private CourseDownloader.UI.Controls.CourseListControl courseListControl1;
        private CourseDownloader.UI.Controls.ModuleListControl moduleListControl1;
        private System.Windows.Forms.ToolStripButton btnAbout;
        private System.Windows.Forms.ToolStripButton btnAddCourse;
        private System.Windows.Forms.ToolStripButton btnPause;
        private System.Windows.Forms.ToolStripButton btnSettings;
        private System.Windows.Forms.ToolStripButton btnBuy;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButton2;
        private System.Windows.Forms.ToolStripMenuItem btnDownloadCompleteCourse;
        private System.Windows.Forms.ToolStripMenuItem btnDownloadSelectedChapters;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripStatusLabel lblChapterName;
        private System.Windows.Forms.ToolStripProgressBar pbDownloadProgress;
        private System.Windows.Forms.ToolStripStatusLabel lblProgress;
        private System.ComponentModel.BackgroundWorker bgwLogin;
        private System.Windows.Forms.ToolStripProgressBar pbLoginProgress;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.CheckBox cbSimulateRealUser;
        private System.Windows.Forms.ToolTip cbToolTip;
        private System.Windows.Forms.ToolStripButton btnLoadCookie;

    }
}

