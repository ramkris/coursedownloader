﻿using CourseDownloader.CourseParser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CourseDownloader.UI.Forms
{
    public partial class LoadCookieForm : Form
    {
        public LoadCookieForm()
        {
            InitializeComponent();
        }

        public ICourseParser Parser
        {
            get;
            set;
        }

        private void btnLoadCookie_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbCookie.Text))
                return;

            Parser.LoadCookie(tbCookie.Text);
            this.Close();
        }
    }
}
