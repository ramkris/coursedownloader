﻿using CourseDownloader.CourseParser;
using CourseDownloader.Model;
using CourseDownloader.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CourseDownloader.UI.Forms
{
    public partial class AddCourseForm : Form
    {
        private Settings settings = Settings.Instance;
        private bool retry;

        public AddCourseForm()
        {
            InitializeComponent();
        }

        public ICourseParser Parser { get; set; }

        public Course Course { get; private set; }

        private void AddCourseForm_Load(object sender, EventArgs e)
        {
            label1.Text = string.Format(label1.Text, settings.Trainer);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbCourseUrl.Text))
                return;

            try
            {
                retry = true;
                OperationInProgress = true;
                Parser.CourseUrl = tbCourseUrl.Text;
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                OperationInProgress = false;
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = Parser.RetrieveCourse();
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                if (retry)
                {
                    retry = false;
                    bgWorker.RunWorkerAsync();
                    return;
                }

                OperationInProgress = false;
                MessageBox.Show(e.Error.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Course = e.Result as Course;
                this.Close();
            }
        }

        private bool OperationInProgress
        {
            set
            {
                if (value)
                {
                    lblStatus.ForeColor = Color.DarkGreen;
                    lblStatus.Text = "Course contents are being retrieved...";
                    Cursor = Cursors.WaitCursor;
                    btnOK.Enabled = false;
                }
                else
                {
                    Cursor = Cursors.Arrow;
                    btnOK.Enabled = true;
                    lblStatus.Text = "";
                }
            }
        }
    }
}
