﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CourseDownloader.Utility;

namespace CourseDownloader.UI.Forms
{
    public partial class AboutForm : Form
    {
        private Settings settings = Settings.Instance;

        public AboutForm()
        {
            InitializeComponent();
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            labelProductName.Text += settings.SoftwareName;
            labelVersion.Text += settings.Version;
            labelWebsite.Text += settings.Website;
            labelEmail.Text += settings.SupportEmail;

            var msg = string.Empty;
            
            if (Helper.IsTrialVersion())
                msg = "You are running trial version of {0}.";
            else
                msg = "Your {0} is licensed. Activation ID: " + settings.LicenseKey;

            msg = string.Format(msg, settings.SoftwareName);

            this.textBoxDescription.Text += settings.SoftwareName + " uses and thanks the creators of the following libraries...";
            this.textBoxDescription.Text += Environment.NewLine;
            this.textBoxDescription.Text += "1.Fizzler";
            this.textBoxDescription.Text += Environment.NewLine;
            this.textBoxDescription.Text += "2.HtmlAgilityPack";
            this.textBoxDescription.Text += Environment.NewLine;
            this.textBoxDescription.Text += "3.Json.NET";
            this.textBoxDescription.Text += Environment.NewLine;
            this.textBoxDescription.Text += "4.LinqBridge";
            this.textBoxDescription.Text += Environment.NewLine;
            this.textBoxDescription.Text += Environment.NewLine;
            this.textBoxDescription.Text += msg;
        }
    }
}
