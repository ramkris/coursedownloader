﻿using CourseDownloader.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CourseDownloader.UI.Forms
{
    public partial class BuyActivateForm : Form
    {
        Settings settings = Settings.Instance;
        bool isValidLicense;

        public BuyActivateForm()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(settings.MojoLink);
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(settings.PaypalLink);
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbActivationId.Text))
                return;

            lblBusy.ForeColor = Color.DarkGreen;
            lblBusy.Text = "Please wait...";
            Cursor = Cursors.WaitCursor;
            btnActivate.Enabled = false;
            tbActivationId.Enabled = false;

            bgWorker.RunWorkerAsync();
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            isValidLicense = Helper.ValidateLicenseKey(tbActivationId.Text);
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblBusy.Text = "";
            Cursor = Cursors.Arrow;
            tbActivationId.Enabled = true;
            btnActivate.Enabled = true;

            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (isValidLicense)
            {
                settings.LicenseKey = tbActivationId.Text;
                this.Close();
                MessageBox.Show("Your license activated successfully.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("The license key you are using is either invalid or is not registered to work with the current Pluralsight ID.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
