﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CourseDownloader.Utility;

namespace CourseDownloader.UI.Forms
{
    public partial class SettingsForm : Form
    {
        private Settings settings = Settings.Instance;

        public SettingsForm()
        {
            InitializeComponent();
        }
        
        private void btnOutputFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult dr = dialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                tbOutputFolder.Text = dialog.SelectedPath;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsFormValid())
            {
                MessageBox.Show("All fields are mandatory.");
                return;
            }

            if (!string.IsNullOrEmpty(tbUserId.Text))
                settings.UserHandle = tbUserId.Text;

            if (!string.IsNullOrEmpty(tbPassword.Text))
                settings.Password = tbPassword.Text;

            if (!string.IsNullOrEmpty(tbOutputFolder.Text))
                settings.OutputFolder = tbOutputFolder.Text;

            this.Close();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            label1.Text = string.Format(label1.Text, settings.Trainer);
            label2.Text = string.Format(label2.Text, settings.Trainer);

            tbUserId.Text = settings.UserHandle;
            tbPassword.Text = settings.Password;
            tbOutputFolder.Text = settings.OutputFolder;
        }

        private bool IsFormValid()
        {
            if (string.IsNullOrEmpty(tbUserId.Text) ||
                string.IsNullOrEmpty(tbPassword.Text) ||
                string.IsNullOrEmpty(tbOutputFolder.Text))
                return false;

            return true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
