﻿namespace CourseDownloader.UI.Forms
{
    partial class LoadCookieForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCookie = new System.Windows.Forms.TextBox();
            this.btnLoadCookie = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbCookie
            // 
            this.tbCookie.Location = new System.Drawing.Point(12, 35);
            this.tbCookie.Multiline = true;
            this.tbCookie.Name = "tbCookie";
            this.tbCookie.Size = new System.Drawing.Size(328, 88);
            this.tbCookie.TabIndex = 0;
            // 
            // btnLoadCookie
            // 
            this.btnLoadCookie.Location = new System.Drawing.Point(265, 129);
            this.btnLoadCookie.Name = "btnLoadCookie";
            this.btnLoadCookie.Size = new System.Drawing.Size(75, 23);
            this.btnLoadCookie.TabIndex = 1;
            this.btnLoadCookie.Text = "Load Cookie";
            this.btnLoadCookie.UseVisualStyleBackColor = true;
            this.btnLoadCookie.Click += new System.EventHandler(this.btnLoadCookie_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Paste the cookie in the below texbox.";
            // 
            // LoadCookieForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 162);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLoadCookie);
            this.Controls.Add(this.tbCookie);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "LoadCookieForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Load Cookie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbCookie;
        private System.Windows.Forms.Button btnLoadCookie;
        private System.Windows.Forms.Label label1;
    }
}