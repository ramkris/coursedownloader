﻿namespace CourseDownloader.UI.Controls
{
    partial class ModuleListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModuleListControl));
            this.lvModules = new CourseDownloader.UI.Controls.DownloaderListViewControl();
            this.chImage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chChapterTitle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnUnmarkChapter = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvModules
            // 
            this.lvModules.CheckBoxes = true;
            this.lvModules.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chImage,
            this.chChapterTitle});
            this.lvModules.ContextMenuStrip = this.contextMenuStrip1;
            this.lvModules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvModules.FullRowSelect = true;
            this.lvModules.Location = new System.Drawing.Point(0, 0);
            this.lvModules.Name = "lvModules";
            this.lvModules.Size = new System.Drawing.Size(454, 275);
            this.lvModules.TabIndex = 0;
            this.lvModules.UseCompatibleStateImageBehavior = false;
            this.lvModules.View = System.Windows.Forms.View.Details;
            this.lvModules.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lvModules_ItemCheck);
            // 
            // chImage
            // 
            this.chImage.Text = "";
            this.chImage.Width = 40;
            // 
            // chChapterTitle
            // 
            this.chChapterTitle.Text = "Chapter Title";
            this.chChapterTitle.Width = 375;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnUnmarkChapter});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(153, 48);
            // 
            // btnUnmarkChapter
            // 
            this.btnUnmarkChapter.Image = ((System.Drawing.Image)(resources.GetObject("btnUnmarkChapter.Image")));
            this.btnUnmarkChapter.Name = "btnUnmarkChapter";
            this.btnUnmarkChapter.Size = new System.Drawing.Size(152, 22);
            this.btnUnmarkChapter.Text = "Unmark";
            this.btnUnmarkChapter.ToolTipText = "Marks the item as not downloaded and deletes the file from the file system";
            this.btnUnmarkChapter.Click += new System.EventHandler(this.btnUnmarkChapter_Click);
            // 
            // ModuleListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvModules);
            this.Name = "ModuleListControl";
            this.Size = new System.Drawing.Size(454, 275);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DownloaderListViewControl lvModules;
        private System.Windows.Forms.ColumnHeader chImage;
        private System.Windows.Forms.ColumnHeader chChapterTitle;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnUnmarkChapter;


    }
}
