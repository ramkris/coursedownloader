﻿namespace CourseDownloader.UI.Controls
{
    partial class CourseListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CourseListControl));
            this.listView1 = new System.Windows.Forms.ListView();
            this.lvCourses = new CourseDownloader.UI.Controls.DownloaderListViewControl();
            this.chImage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chChapterTitle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTotalChaptersCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chDownloadedChaptersCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnRemoveCourse = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(525, 256);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // lvCourses
            // 
            this.lvCourses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chImage,
            this.chChapterTitle,
            this.chTotalChaptersCount,
            this.chDownloadedChaptersCount});
            this.lvCourses.ContextMenuStrip = this.contextMenuStrip1;
            this.lvCourses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvCourses.FullRowSelect = true;
            this.lvCourses.Location = new System.Drawing.Point(0, 0);
            this.lvCourses.MultiSelect = false;
            this.lvCourses.Name = "lvCourses";
            this.lvCourses.Size = new System.Drawing.Size(525, 256);
            this.lvCourses.TabIndex = 1;
            this.lvCourses.UseCompatibleStateImageBehavior = false;
            this.lvCourses.View = System.Windows.Forms.View.Details;
            this.lvCourses.SelectedIndexChanged += new System.EventHandler(this.lvCourses_SelectedIndexChanged);
            // 
            // chImage
            // 
            this.chImage.Text = "";
            this.chImage.Width = 25;
            // 
            // chChapterTitle
            // 
            this.chChapterTitle.Text = "Title";
            this.chChapterTitle.Width = 350;
            // 
            // chTotalChaptersCount
            // 
            this.chTotalChaptersCount.Text = "Total";
            this.chTotalChaptersCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chTotalChaptersCount.Width = 65;
            // 
            // chDownloadedChaptersCount
            // 
            this.chDownloadedChaptersCount.Text = "Downloaded";
            this.chDownloadedChaptersCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chDownloadedChaptersCount.Width = 75;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRemoveCourse});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(158, 26);
            // 
            // btnRemoveCourse
            // 
            this.btnRemoveCourse.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveCourse.Image")));
            this.btnRemoveCourse.Name = "btnRemoveCourse";
            this.btnRemoveCourse.Size = new System.Drawing.Size(157, 22);
            this.btnRemoveCourse.Text = "Remove Course";
            this.btnRemoveCourse.Click += new System.EventHandler(this.btnRemoveCourse_Click);
            // 
            // CourseListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvCourses);
            this.Controls.Add(this.listView1);
            this.Name = "CourseListControl";
            this.Size = new System.Drawing.Size(525, 256);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private DownloaderListViewControl lvCourses;
        private System.Windows.Forms.ColumnHeader chImage;
        private System.Windows.Forms.ColumnHeader chChapterTitle;
        private System.Windows.Forms.ColumnHeader chTotalChaptersCount;
        private System.Windows.Forms.ColumnHeader chDownloadedChaptersCount;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnRemoveCourse;
    }
}
