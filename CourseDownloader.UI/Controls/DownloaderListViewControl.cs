﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace CourseDownloader.UI.Controls
{
    public class DownloaderListViewControl : ListView
    {
        private ListViewItem downloadingItem;

        public DownloaderListViewControl()
        {
            SmallImageList = new ImageList();
            //SmallImageList.Images.Add(Bitmap.FromFile(".\\Icons\\notstarted.ico"));
            //SmallImageList.Images.Add(Bitmap.FromFile(".\\Icons\\downloaded.ico"));
            SmallImageList.Images.Add(Properties.Resources.notstarted);
            SmallImageList.Images.Add(Properties.Resources.downloaded);
        }

        public ListViewItem DownloadingItem
        {
            set
            {
                //Debug.Assert(downloadingItem != value);

                if (downloadingItem != null)
                {
                    downloadingItem.Font = OriginalFont;
                    downloadingItem.BackColor = Color.White;
                    downloadingItem.ForeColor = Color.Black;
                }

                if (value != null)
                {
                    downloadingItem = value;
                    downloadingItem.Font = new Font(OriginalFont.FontFamily, OriginalFont.Size, FontStyle.Bold);
                    downloadingItem.ForeColor = Color.DarkGreen;
                    downloadingItem.BackColor = Color.WhiteSmoke;
                }
            }
        }

        public ListViewItem DownloadedItem
        {
            set
            {
                //Debug.Assert(downloadingItem != null);
                //Debug.Assert(downloadingItem == value);

                if (value != null)
                    value.ImageIndex = 1;

                if (downloadingItem != null)
                {
                    downloadingItem.Font = OriginalFont;
                    downloadingItem.BackColor = Color.White;
                    downloadingItem.ForeColor = Color.Black;
                }
            }
        }

        private ListViewItem _item;
        private Font OriginalFont
        {
            get
            {
                if (_item == null)
                    _item = new ListViewItem();

                return _item.Font;
            }
        }
    }
}
