﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CourseDownloader.Model;
using CourseDownloader.Utility;
using System.IO;
using System.Diagnostics;

namespace CourseDownloader.UI.Controls
{
    public partial class ModuleListControl : UserControl
    {
        private List<Module> modules = new List<Module>();
        private Chapter downloadingChapter;
        private Downloader downloader = Downloader.Instance;

        public ModuleListControl()
        {
            InitializeComponent();
            downloader.CourseDownloadPaused += downloader_CourseDownloadPaused;
            downloader.ChapterDownloadStarted += downloader_ChapterDownloadStarted;
            downloader.ChapterDownloadCompleted += downloader_ChapterDownloadCompleted;
        }

        public List<Module> Modules
        {
            set
            {
                lvModules.Items.Clear();
                lvModules.Groups.Clear();

                if (value == null)
                    modules = new List<Module>();
                else
                    modules = value;

                PopulateModuleListView();
            }
        }

        void downloader_CourseDownloadPaused(Course course)
        {
            Invoke((MethodInvoker)delegate
            {
                //Debug.Assert(downloadingChapter != null);
                downloadingChapter = null;
                lvModules.DownloadingItem = null;
            });
        }

        void downloader_ChapterDownloadStarted(Chapter chapter)
        {
            Invoke((MethodInvoker)delegate
            {
                //Debug.Assert(downloadingChapter == null);
                //Debug.Assert(downloadingChapter != chapter);

                downloadingChapter = chapter;
                var lvChapterItem = lvModules.Items[chapter.Id];
                lvModules.DownloadingItem = lvChapterItem;
            });
        }

        void downloader_ChapterDownloadCompleted(Chapter chapter)
        {
            Invoke((MethodInvoker)delegate
            {
                Debug.Assert(downloadingChapter != null);
                Debug.Assert(downloadingChapter == chapter);

                var lvChapterItem = lvModules.Items[downloadingChapter.Id];
                lvModules.DownloadedItem = lvChapterItem;

                if (lvChapterItem != null)
                    lvChapterItem.Checked = false;

                downloadingChapter = null;
            });
        }

        public List<Chapter> SelectedChapters
        {
            get
            {
                var checkedItems = lvModules.CheckedItems;
                var selectedChapters = new List<Chapter>();

                foreach (var module in modules)
                {
                    foreach (var chapter in module.Chapters)
                    {
                        if (checkedItems.ContainsKey(chapter.Id))
                            selectedChapters.Add(chapter);
                    }
                }

                return selectedChapters;
            }
        }

        public List<Chapter> AllChapters
        {
            get
            {
                var allChapters = new List<Chapter>();

                foreach (var module in modules)
                {
                    foreach (var chapter in module.Chapters)
                    {
                        allChapters.Add(chapter);
                    }
                }

                foreach (ListViewItem item in lvModules.Items)
                    item.Checked = true;

                return allChapters;
            }
        }

        private void PopulateModuleListView()
        {
            if (modules == null)
                return;

            foreach (var module in modules)
            {
                var moduleGroup = new ListViewGroup(module.Title, module.Title);
                lvModules.Groups.Add(moduleGroup);
                foreach (var chapter in module.Chapters)
                {
                    var imgIndex = chapter.IsDownloaded ? 1 : 0;
                    var lvChapterItem = lvModules.Items.Add(chapter.Id, string.Empty, imgIndex);
                    lvChapterItem.Tag = chapter.Id;
                    lvChapterItem.Group = moduleGroup;
                    lvChapterItem.SubItems.Add(chapter.Title);
                    lvChapterItem.Checked = downloader.DownloadingChapters.Exists((c) => c == chapter);

                    if (downloader.DownloadingChapter == chapter)
                    {
                        downloadingChapter = chapter;
                        lvModules.DownloadingItem = lvChapterItem;
                    }
                }
            }
        }

        private void btnUnmarkChapter_Click(object sender, EventArgs e)
        {
            if (lvModules.SelectedItems.Count == 0)
            {
                MessageBox.Show("Select a chapter to unmark download status", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (lvModules.SelectedItems.Count > 1)
            {
                MessageBox.Show("You can only select one chapter. Make sure you have only one chapter selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var lvItem = lvModules.SelectedItems[0];
            var chapter = GetChapterFromListViewItem(lvItem);
            chapter.IsDownloaded = false;
            lvItem.ImageIndex = 0;

            var file = Helper.MakeChapterFileName(chapter.FileNameParts);

            if (File.Exists(file))
                File.Delete(file);
        }

        private void lvModules_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var lvItem = lvModules.Items[e.Index];
            var chapter = GetChapterFromListViewItem(lvItem);

            if (chapter.IsDownloaded)
                e.NewValue = CheckState.Unchecked;
        }

        private Chapter GetChapterFromListViewItem(ListViewItem lvItem)
        {
            foreach (var module in modules)
            {
                foreach (var chapter in module.Chapters)
                {
                    if (chapter.Id == (string)lvItem.Tag)
                        return chapter;
                }
            }

            return null;
        }
    }
}
