﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CourseDownloader.Model;
using CourseDownloader.Utility;
using System.Diagnostics;

namespace CourseDownloader.UI.Controls
{
    public delegate void CourseSelectedHandler(Course selectedCourse);

    public partial class CourseListControl : UserControl
    {
        public event CourseSelectedHandler CourseSelected;

        private Course downloadingCourse;
        private List<Course> courses;
        private Downloader downloader = Downloader.Instance;

        public CourseListControl()
        {
            InitializeComponent();
            downloader.CourseDownloadStarted += downloader_CourseDownloadStarted;
            downloader.CourseDownloadPaused += downloader_CourseDownloadPaused;
            downloader.CourseDownloadCompleted += downloader_CourseDownloadCompleted;
            downloader.ChapterDownloadCompleted += downloader_ChapterDownloadCompleted;
        }

        public List<Course> Courses
        {
            get
            {
                return courses;
            }
        }

        public Course SelectedCourse
        {
            get;
            private set;
        }

        public void AddCourse(Course course)
        {
            Debug.Assert(courses != null);

            courses.Add(course);
            var item = AddListViewItem(course);
            item.Selected = true;
        }

        public void PopulateCourseList()
        {
            lvCourses.Items.Clear();
            courses = Helper.ReadDataFile();

            foreach (var course in courses)
            {
                AddListViewItem(course);
            }

            if (lvCourses.Items.Count > 0)
                lvCourses.Items[0].Selected = true;
        }

        void downloader_ChapterDownloadCompleted(Chapter chapter)
        {
            Invoke((MethodInvoker)delegate
            {
                Debug.Assert(downloadingCourse != null);

                var lvCourseItem = lvCourses.Items[downloadingCourse.Id];
                var counter = downloadingCourse.DownloadedChaptersCount;
                lvCourseItem.SubItems[3].Text = counter.ToString();
            });
        }

        void downloader_CourseDownloadPaused(Course course)
        {
            Invoke((MethodInvoker)delegate
            {
                Debug.Assert(downloadingCourse != null);
                Debug.Assert(downloadingCourse == course);

                lvCourses.DownloadingItem = null;
                downloadingCourse = null;
            });
        }

        void downloader_CourseDownloadCompleted(Course course)
        {
            Invoke((MethodInvoker)delegate
            {
                Debug.Assert(downloadingCourse != null);
                Debug.Assert(downloadingCourse == course);

                downloadingCourse = null;
                var lvCourseItem = lvCourses.Items[course.Id];

                if (course.IsDownloaded)
                    lvCourses.DownloadedItem = lvCourseItem;
                else
                    lvCourses.DownloadedItem = null;
            });
        }

        void downloader_CourseDownloadStarted(Course course)
        {
            Invoke((MethodInvoker)delegate
            {
                Debug.Assert(downloadingCourse == null);

                downloadingCourse = course;
                var lvCourseItem = lvCourses.Items[course.Id];
                lvCourses.DownloadingItem = lvCourseItem;
            });
        }

        private ListViewItem AddListViewItem(Course course)
        {
            var imgIndex = course.IsDownloaded ? 1 : 0;
            var lvCourseItem = lvCourses.Items.Insert(0, course.Id, string.Empty, imgIndex);
            lvCourseItem.Tag = course.Id;
            lvCourseItem.SubItems.Add(course.Title);
            lvCourseItem.SubItems.Add(course.TotalChaptersCount.ToString());
            lvCourseItem.SubItems.Add(course.DownloadedChaptersCount.ToString());

            return lvCourseItem;
        }

        private void lvCourses_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvCourses.SelectedItems.Count == 0)
                return;

            var lvCourseItem = lvCourses.SelectedItems[0];
            var course = courses.Find(c => c.Id == (string)lvCourseItem.Tag);
            SelectedCourse = course;
            CourseSelected(course);
        }


        public Action<Course> OnCourseRemoved
        {
            private get;
            set;
        }

        private void btnRemoveCourse_Click(object sender, EventArgs e)
        {
            if (lvCourses.SelectedItems.Count == 0)
            {
                MessageBox.Show("Select a course to remove", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (lvCourses.SelectedItems.Count > 1)
            {
                MessageBox.Show("You can only select one course. Make sure you have only one course selected.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var lvCourseItem = lvCourses.SelectedItems[0];
            var course = courses.Find(c => c.Id == (string)lvCourseItem.Tag);

            if (course == downloadingCourse)
            {
                MessageBox.Show("This course is being downloaded. You need to pause the download or wait for the download to complete before removing the course.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            courses.Remove(course);
            lvCourses.Items.Remove(lvCourseItem);

            if (OnCourseRemoved != null)
                OnCourseRemoved(course);
        }
    }
}
