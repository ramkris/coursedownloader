﻿using System;
using System.Collections.Generic;
using System.Text;
using CourseDownloader.Model;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.Net;

namespace CourseDownloader.Utility
{
    public static class Helper
    {
        private static Settings settings;

        static Helper()
        {
            settings = Settings.Instance;
        }

        public static List<Course> ReadDataFile()
        {
            var allCourses = new List<Course>();

            if (!File.Exists(settings.DataFile))
                return allCourses;

            using (BinaryReader br = new BinaryReader(File.OpenRead(settings.DataFile)))
            {
                using (BsonReader bsreader = new BsonReader(br))
                {
                    bsreader.ReadRootValueAsArray = true;
                    JsonSerializer serializer = new JsonSerializer();
                    allCourses = serializer.Deserialize<List<Course>>(bsreader);
                }
            }

            if (allCourses == null)
                allCourses = new List<Course>();

            return allCourses;
        }

        public static void SaveDataFile(List<Course> allCourses)
        {
            using (BinaryWriter bw = new BinaryWriter(File.Open(settings.DataFile, FileMode.Create)))
            {
                using (BsonWriter writer = new BsonWriter(bw))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(writer, allCourses);
                }
            }
        }

        public static void CreateDataFolder()
        {
            if (!Directory.Exists(settings.DataFolder))
                Directory.CreateDirectory(settings.DataFolder);
        }

        public static string RemoveSpecialCharacters(string fileName)
        {
            foreach (var ch in Path.GetInvalidPathChars())
                fileName = fileName.Replace(ch, ' ');

            foreach (var ch in Path.GetInvalidFileNameChars())
                fileName = fileName.Replace(ch, ' ');

            return fileName;
        }

        public static string MakeChapterFileName(string[] chapterFileNameParts)
        {
            var outputFolder = settings.OutputFolder;
            var courseFolder = RemoveSpecialCharacters(chapterFileNameParts[0]);
            var moduleFolder = RemoveSpecialCharacters(chapterFileNameParts[1]);
            var chapterName = RemoveSpecialCharacters(chapterFileNameParts[2]);

            var absoluteModuleFolder = Path.Combine(Path.Combine(outputFolder, courseFolder), moduleFolder);
            absoluteModuleFolder = absoluteModuleFolder.Replace("  ", " ");

            if (!Directory.Exists(absoluteModuleFolder))
                Directory.CreateDirectory(absoluteModuleFolder);

            var fileName = Path.Combine(absoluteModuleFolder, chapterName.Replace("  ", " ") + ".mp4");
            

            return fileName;
        }

        public static bool IsTrialVersion()
        {
            if (settings.LicenseKey == settings.TrialKey)
                return true;

            return false;
        }

        public static bool IsUserConfigSetup()
        {
            if (string.IsNullOrEmpty(settings.UserHandle) ||
                string.IsNullOrEmpty(settings.Password) ||
                string.IsNullOrEmpty(settings.OutputFolder))
                return false;

            return true;
        }

        public static bool ValidateLicenseKey(string licenseKey)
        {
            var status = string.Empty;
            var url = "{0}/home/VerifyLicense?psUserHandle={1}&activationId={2}";
            url = string.Format(url, settings.Website, settings.UserHandle, licenseKey);

            using (WebClient client = new WebClient())
            {
                status = client.DownloadString(url);
            }

            if (status == "true")
                return true;

            return false;
        }
    }
}
