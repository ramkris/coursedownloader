﻿using CourseDownloader.Model;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Diagnostics;
using CourseDownloader.CourseParser;
using System.Windows.Forms;

namespace CourseDownloader.Utility
{
    public delegate void CourseDownloadStartedHandler(Course course);
    public delegate void CourseDownloadPausedHandler(Course course);
    public delegate void CourseDownloadCompletedHandler(Course course);
    public delegate void ChapterDownloadStartedHandler(Chapter chapter);
    public delegate void ChapterDownloadCompletedHandler(Chapter chapter);
    public delegate void DownloadProgressChangedHandler(int progress);

    public class Downloader
    {
        private static Downloader _instance;

        private WebClient webClient;
        private Thread downloadThread;
        private Settings settings = Settings.Instance;
        private Stopwatch stopWatch = new Stopwatch();
        private ManualResetEvent downloadDoneEvent = new ManualResetEvent(false);

        public event CourseDownloadStartedHandler CourseDownloadStarted;
        public event CourseDownloadPausedHandler CourseDownloadPaused;
        public event CourseDownloadCompletedHandler CourseDownloadCompleted;
        public event ChapterDownloadStartedHandler ChapterDownloadStarted;
        public event ChapterDownloadCompletedHandler ChapterDownloadCompleted;
        public event DownloadProgressChangedHandler DownloadProgressChanged;

        private Downloader()
        {
            webClient = new WebClient();
            webClient.DownloadProgressChanged += webClient_DownloadProgressChanged;
            webClient.DownloadFileCompleted += webClient_DownloadFileCompleted;
            DownloadingChapters = new List<Chapter>();
        }

        void webClient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null && !(e.Error is WebException))
            {
                PauseDownload();
                var msg = e.Error.Message;
                msg += Environment.NewLine;
                msg += Environment.NewLine;
                msg += "Download has been paused.";
                MessageBox.Show(msg, "Error Occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (e.Error == null && !e.Cancelled)
            {
                var chapter = e.UserState as Chapter;
                chapter.IsDownloaded = true;

                if (ChapterDownloadCompleted != null)
                    ChapterDownloadCompleted(chapter);
            }

            downloadDoneEvent.Set();
        }

        void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            if (DownloadProgressChanged != null)
                DownloadProgressChanged(e.ProgressPercentage);
        }

        public static Downloader Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Downloader();

                return _instance;
            }
        }

        public Course DownloadingCourse
        {
            get;
            private set;
        }

        public ICourseParser Parser
        {
            get;
            set;
        }

        public bool IsBusy
        {
            get;
            private set;
        }

        public List<Chapter> DownloadingChapters
        {
            get;
            private set;
        }

        public Chapter DownloadingChapter
        {
            get;
            private set;
        }

        public void PauseDownload()
        {
            if (!IsBusy)
                return;

            Debug.Assert(webClient != null);
            webClient.CancelAsync();

            Debug.Assert(downloadThread != null);
            downloadThread.Abort();
        }

        public void DownloadChapters(List<Chapter> chapters, Course course)
        {
            //ValidateAndResetLicense();

            downloadThread = new Thread(new ThreadStart(() =>
            {
                try
                {
                    Debug.Assert(DownloadingChapters.Count == 0);
                    Debug.Assert(DownloadingCourse == null);
                    Debug.Assert(!IsBusy);

                    IsBusy = true;
                    DownloadingCourse = course;
                    DownloadingChapters.RemoveRange(0, DownloadingChapters.Count);
                    DownloadingChapters.AddRange(chapters);

                    if (CourseDownloadStarted != null)
                        CourseDownloadStarted(DownloadingCourse);

                    var chapterNo = 0;

                    while (chapterNo < chapters.Count)
                    {
                        var chapter = chapters[chapterNo];

                        //if (HasReachedLimit())
                        //    break;

                        if (chapter.IsDownloaded)
                        {
                            DownloadingChapters.Remove(chapter);
                            chapterNo++;
                            continue;
                        }

                        if (ChapterDownloadStarted != null)
                            ChapterDownloadStarted(chapter);

                        DownloadingChapter = chapter;

                        if (settings.SimulateRealUser)
                        {
                            stopWatch.Reset();
                            stopWatch.Start();
                            DownloadChapter(chapter);
                            stopWatch.Stop();
                            var waitDuration = chapter.TotalMilliSeconds - (int)stopWatch.ElapsedMilliseconds;

                            if (waitDuration > 0 && chapter.IsDownloaded)
                                Thread.Sleep(waitDuration);
                        }
                        else
                        {
                            DownloadChapter(chapter);
                        }
                    }

                    if (CourseDownloadCompleted != null)
                        CourseDownloadCompleted(DownloadingCourse);
                }
                catch (Exception ex)
                {
                    if (!(ex is ThreadAbortException))
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    if (CourseDownloadPaused != null)
                        CourseDownloadPaused(DownloadingCourse);
                }
                finally
                {
                    Reset();
                }
            }));
            downloadThread.IsBackground = true;
            downloadThread.Start();
        }

        private void DownloadChapter(Chapter chapter)
        {
            var chapterUrl = new Uri(Parser.GetChapterUrl(chapter.Tokens));
            var fileName = Helper.MakeChapterFileName(chapter.FileNameParts);

            webClient.Headers.Add(HttpRequestHeader.UserAgent, settings.UserAgent);
            webClient.DownloadFileAsync(chapterUrl, fileName, chapter);
            downloadDoneEvent.Reset();
            downloadDoneEvent.WaitOne();
        }

        private void Reset()
        {
            DownloadingChapter = null;
            DownloadingCourse = null;
            IsBusy = false;
            DownloadingChapters.RemoveRange(0, DownloadingChapters.Count);
        }

        private bool HasReachedLimit()
        {
            if (Helper.IsTrialVersion() && DownloadingCourse.DownloadedChaptersCount >= 2)
            {
                var msg = "Trial version can download a maximum of 2 videos per course.";
                MessageBox.Show(msg, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            return false;
        }

        public Action<object> OnLicenseReset
        {
            private get;
            set;
        }

        private void ValidateAndResetLicense()
        {
            try
            {
                if (!Helper.IsTrialVersion() && !Helper.ValidateLicenseKey(settings.LicenseKey))
                {
                    settings.LicenseKey = settings.TrialKey;
                    MessageBox.Show("The license key you are using is either invalid or is not registered to work with the current Pluralsight ID.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    if (OnLicenseReset != null)
                        OnLicenseReset(null);
                }
            }
            catch
            {
            }
        }
    }
}
