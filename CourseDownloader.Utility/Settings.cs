﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Windows.Forms;
using System.IO;

namespace CourseDownloader.Utility
{
    public class Settings
    {
        private static Settings _instance;

        private Configuration config;
        private KeyValueConfigurationCollection settings;

        private Settings()
        {
            config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            settings = config.AppSettings.Settings;
        }

        public static Settings Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Settings();

                return _instance;
            }
        }

        public string UserAgent
        {
            get
            {
                return "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
            }
        }

        public string UserHandle
        {
            get
            {
                return settings["userhandle"].Value;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    return;

                settings["userhandle"].Value = value;
                config.Save();
            }
        }

        public string Password
        {
            get
            {
                return settings["password"].Value;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    return;

                settings["password"].Value = value;
                config.Save();
            }
        }

        public string OutputFolder
        {
            get
            {
                var outputFolder = settings["outputfolder"].Value;

                if (string.IsNullOrEmpty(outputFolder) || !Directory.Exists(outputFolder))
                {
                    outputFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    settings["outputfolder"].Value = outputFolder;
                    config.Save();
                }

                return outputFolder;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    return;

                settings["outputfolder"].Value = value;
                config.Save();
            }
        }

        public string LicenseKey
        {
            get
            {
                return settings["licensekey"].Value;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    return;

                settings["licensekey"].Value = value;
                config.Save();
            }
        }

        public bool SimulateRealUser
        {
            get
            {
                return bool.Parse(settings["simulaterealuser"].Value);
            }
            set
            {
                var val = value.ToString();
                settings["simulaterealuser"].Value = val;
                config.Save();
            }
        }

        public string DataFile
        {
            get
            {
                return Path.Combine(DataFolder, settings["datafile"].Value);
            }
        }

        public string DataFolder
        {
            get
            {
                var dataFolder = settings["datafolder"].Value;
                var myDocsFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                return Path.Combine(myDocsFolder, dataFolder);
            }
        }

        public string Website
        {
            get
            {
                return settings["website"].Value;
            }
        }

        public string SupportEmail
        {
            get
            {
                return settings["email"].Value;
            }
        }

        public string TrialKey
        {
            get
            {
                return "42A4AEA5-2DA9-407B-9070-08C830834C3A";
            }
        }

        public string Version
        {
            get
            {
                return settings["version"].Value;
            }
        }

        public string Trainer
        {
            get
            {
                return settings["trainer"].Value;
            }
        }

        public string MojoLink
        {
            get
            {
                return settings["mojolink"].Value;
            }
        }

        public string PaypalLink
        {
            get
            {
                return settings["paypallink"].Value;
            }
        }

        public string SoftwareName
        {
            get
            {
                return settings["softwarename"].Value;
            }
        }
    }
}
