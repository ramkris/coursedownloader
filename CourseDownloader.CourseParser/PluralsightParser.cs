﻿using Fizzler.Systems.HtmlAgilityPack;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using CourseDownloader.Model;
using System.Threading;
using Newtonsoft.Json;

namespace CourseDownloader.CourseParser
{
    public class PluralsightParser : ICourseParser
    {
        private WebClientEx webClient;
        private HtmlDocument htmlDoc;
        private string courseUrl;
        private ManualResetEvent isBusyEvent = new ManualResetEvent(true);

        private const string PSLOGINURL = "https://app.pluralsight.com/id?redirectTo=https%3a%2f%2fapp.pluralsight.com%2flibrary%2f"; //"https://app.pluralsight.com/id?redirectTo=http%3a%2f%2fwww.pluralsight.com%2f";
        private const string PSVIEWCLIPURL = "https://app.pluralsight.com/player/retrieve-url";
        private const string PSLOGOUTURL = "https://www.pluralsight.com/training/Login/Logout?returnUrl=%2Ftraining";

        public PluralsightParser()
        {
            webClient = new WebClientEx();
            webClient.RedirectFlag = true;
            htmlDoc = new HtmlDocument();
        }

        public bool IsLoggedIn { get; private set; }

        public string CourseUrl
        {
            get
            {
                return courseUrl;
            }
            set
            {
                var url = value.ToLower();

                if (!url.Contains("pluralsight.com/courses"))
                    throw new Exception("The url doesn't appear to be a valid pluralsight url.");

                courseUrl = url;
            }
        }

        public bool Login(string userHandle, string password)
        {
            try
            {
                IsLoggedIn = true;
                //return IsLoggedIn;

                isBusyEvent.WaitOne();
                isBusyEvent.Reset();

                AddStandardHttpHeaders();
                var loginPage = webClient.DownloadString(PSLOGINURL);
                htmlDoc.LoadHtml(loginPage);

                var loginForm = htmlDoc.DocumentNode.QuerySelectorAll(".signin input").ToList();
                var loginFields = new NameValueCollection();

                foreach (var loginField in loginForm)
                {
                    var attrName = loginField.GetAttributeValue("name", "");
                    var attrValue = loginField.GetAttributeValue("value", "");

                    loginFields.Add(attrName, attrValue);
                }

                loginFields["Username"] = userHandle;
                loginFields["Password"] = password;

                AddStandardHttpHeaders();
                loginFields["ReturnUrl"] = "http://www.pluralsight.com/";
                var bytes = webClient.UploadValues(PSLOGINURL, loginFields);
                var loginResponse = UTF8Encoding.UTF8.GetString(bytes);

                htmlDoc.LoadHtml(loginResponse);
                //IsLoggedIn = htmlDoc.DocumentNode.QuerySelectorAll(".signedIn").ToList().Count > 0;
                IsLoggedIn = !loginResponse.Contains("Forgot Password?");

                if (!IsLoggedIn)
                    throw new Exception("Login failed.");

                return IsLoggedIn;
            }
            finally
            {
                isBusyEvent.Set();
            }
        }

        public Course RetrieveCourse()
        {
            try
            {
                isBusyEvent.WaitOne();
                isBusyEvent.Reset();

                if (!IsLoggedIn)
                    throw new Exception("You are not logged in.");

                if (string.IsNullOrEmpty(courseUrl))
                    throw new Exception("Course URL is empty.");

                AddStandardHttpHeaders();

                var courseUrlParts = courseUrl.Split('/');
                var courseSlug = courseUrlParts[courseUrlParts.Length - 1];
                var courseTocUrl = "http://www.pluralsight.com/data/Course/Content/" + courseSlug;
                var courseMetadataUrl = "http://www.pluralsight.com/data/Course/" + courseSlug;

                var courseMetadata = webClient.DownloadString(courseMetadataUrl);
                var courseTocData = webClient.DownloadString(courseTocUrl);

                var courseModules = JsonConvert.DeserializeObject<List<PSModule>>(courseTocData);
                var courseInfo = JsonConvert.DeserializeObject<PSCourse>(courseMetadata);

                var course = new Course();
                course.Url = courseUrl;
                course.Title = courseInfo.Title;
                course.Modules = new List<Module>();
                var moduleNo = 1;

                foreach (var m in courseModules)
                {
                    var module = new Module();
                    module.Title = m.Title;
                    module.Chapters = new List<Chapter>();

                    var chapterNo = 1;
                    foreach (var c in m.Clips)
                    {
                        var chapter = CreateChapter(c);
                        chapter.FileNameParts = new string[3];
                        chapter.FileNameParts[0] = course.Title;
                        chapter.FileNameParts[1] = moduleNo + "." + module.Title;
                        chapter.FileNameParts[2] = chapterNo + "." + chapter.Title;
                        module.Chapters.Add(chapter);
                        chapterNo++;
                    }
                    course.Modules.Add(module);
                    moduleNo++;
                }

                if (course.TotalChaptersCount <= 0)
                    throw new Exception("Failed to retrieve course contents.");

                return course;
            }
            finally
            {
                isBusyEvent.Set();
            }
        }

        public string GetChapterUrl(Dictionary<string, string> tokens)
        {
            try
            {
                isBusyEvent.WaitOne();
                isBusyEvent.Reset();

                if (!IsLoggedIn)
                    throw new Exception("You are not logged in.");

                var requestPayload = string.Empty;

                requestPayload += "{";
                requestPayload += "\"author\":" + "\"" + tokens["Author"] + "\", ";
                requestPayload += "\"moduleName\":" + "\"" + tokens["Name"] + "\", ";
                requestPayload += "\"courseName\":" + "\"" + tokens["Course"] + "\", ";
                requestPayload += "\"clipIndex\":" + tokens["Clip"] + ", ";
                requestPayload += "\"mediaType\":" + "\"" + "mp4" + "\", ";
                requestPayload += "\"quality\":" + "\"" + "1024x768" + "\", ";
                requestPayload += "\"includeCaption\":" + "false" + ", ";
                requestPayload += "\"locale\":" + "\"" + "en" + "\"";
                requestPayload += "}";

                AddStandardHttpHeaders();
                var postBytes = Encoding.UTF8.GetBytes(requestPayload);
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json;charset=UTF-8");
                webClient.Headers.Add(HttpRequestHeader.Accept, "application/json, text/plain, */*");
                //webClient.Headers.Add(HttpRequestHeader.Cookie, "PSM=ABDB9B950AD5F4A925980918AE8CF0039BC0800C973A8DEC1A3B3D84FAF0C2E0461FAF739BCA6B60E25AAEBAF69CD5B646DE36BC9EAEE2DE4D831F46E52E80F6CD8BF3E5E6F22DA0320A606DDDD342487D33A47F3FAD4885074C521F94B172403A8A5164FFBD125E0B9A8D640ADFE7FE1B89AA4D23E5DE19F0FBC98DF81261600EB594D115B2FC1EF0D5938DF5B7236819801DC066BC7E20BF7CDE91C8E771D03F2392D407903CAA6D4034EC029B1B5CE4BE0F8F; domain=pluralsight.com; expires=Fri, 05-Aug-2016 07:48:38 GMT; path=/");
                var receivedBytes = webClient.UploadData(PSVIEWCLIPURL, postBytes);
                var chapterUrlCollectionString = Encoding.UTF8.GetString(receivedBytes);
                webClient.Headers.Remove(HttpRequestHeader.ContentType);

                var chapterUrlCollection = JsonConvert.DeserializeObject<ChapterUrlCollection>(chapterUrlCollectionString);

                return chapterUrlCollection.Urls[0].Url;
            }
            finally
            {
                isBusyEvent.Set();
            }
        }

        public void Logout()
        {
            try
            {
                if (!IsLoggedIn)
                    throw new Exception("You are not logged in.");

                //AddStandardHttpHeaders();
                //webClient.UploadValues(PSLOGOUTURL, new NameValueCollection());
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void LoadCookie(string cookieString)
        {
            if (cookieString.StartsWith("Cookie:", StringComparison.OrdinalIgnoreCase))
                cookieString = cookieString.Replace("Cookie:", string.Empty);

            var tokens = cookieString.Split(';');

            foreach (var token in tokens)
            {
                var equalFirstIndex = token.IndexOf('=');
                var cookieKey = token.Substring(0, equalFirstIndex);
                var cookieValue = token.Substring(equalFirstIndex + 1);

                cookieKey = cookieKey.Trim();
                cookieValue = cookieValue.Trim();

                var cookie = new Cookie(cookieKey, cookieValue);
                cookie.Domain = "pluralsight.com";
                webClient.CookieContainer.Add(cookie);
            }

            IsLoggedIn = true;
        }

        private static Chapter CreateChapter(PSClip clip)
        {
            var chapter = new Chapter();

            chapter.Title = clip.Title;
            chapter.Tokens = new Dictionary<string, string>();
            chapter.TotalMilliSeconds = GetDurationInMilliseconds(clip.Duration);

            foreach (var param in clip.PlayerParameters.Split('&'))
            {
                var keyValue = param.Split('=');
                var key = keyValue[0].ToLower();
                var value = keyValue[1].ToLower();

                if (key == "author")
                    chapter.Tokens["Author"] = value;

                if (key == "name")
                    chapter.Tokens["Name"] = value;

                if (key == "mode")
                    chapter.Tokens["Mode"] = value;

                if (key == "clip")
                    chapter.Tokens["Clip"] = value;

                if (key == "course")
                    chapter.Tokens["Course"] = value;
            }

            return chapter;
        }

        private static int GetDurationInMilliseconds(string duration)
        {
            var durationParts = duration.Split(':');
            var milliSecondsDuration = 0;
            var multiplier = 1;

            for (var i = durationParts.Length - 1; i >= 0; i--)
            {
                milliSecondsDuration += int.Parse(durationParts[i]) * multiplier;
                multiplier *= 60;
            }

            return milliSecondsDuration * 1000;
        }

        private void AddStandardHttpHeaders()
        {
            webClient.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
        }

        class PSCourse
        {
            public string Title { get; set; }
            public string Description { get; set; }
        }

        class PSModule
        {
            public string Title { get; set; }
            public List<PSClip> Clips { get; set; }
        }

        class PSClip
        {
            public string Title { get; set; }
            public string Duration { get; set; }
            public string PlayerParameters { get; set; }
        }

        class ChapterUrlCollection
        {
            public List<ChapterUrl> Urls { get; set; }
            public string Captions { get; set; }
        }

        class ChapterUrl
        {
            public int Rank { get; set; }
            public string Cdn { get; set; }
            public string Url { get; set; }
        }
    }
}
