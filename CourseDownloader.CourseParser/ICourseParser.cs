﻿using CourseDownloader.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseDownloader.CourseParser
{
    public interface ICourseParser
    {
        string CourseUrl { get; set; }

        bool IsLoggedIn { get; }

        bool Login(string userHandle, string password);

        Course RetrieveCourse();

        string GetChapterUrl(Dictionary<string, string> tokens);

        void LoadCookie(string cookie);

        void Logout();
    }
}
