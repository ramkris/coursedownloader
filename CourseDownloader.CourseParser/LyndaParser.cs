﻿using Fizzler.Systems.HtmlAgilityPack;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using CourseDownloader.Model;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json;
using System.Threading;
using System.Diagnostics;

namespace CourseDownloader.CourseParser
{
    public class LyndaParser : ICourseParser
    {
        private string courseUrl;
        private WebClientEx webClient;
        private HtmlDocument htmlDoc;
        private Dictionary<string, string> cookieDictionary;
        private ManualResetEvent isBusyEvent = new ManualResetEvent(true);

        private const string LYNDALOGINPAGE = "http://www.lynda.com/user/login/modal";
        private const string LYNDALOGINSUBMITPAGE = "https://www.lynda.com/login/login.aspx";
        private const string LYNDAVIEWCLIPURL = "http://www.lynda.com/ajax/player?videoId={0}&type=video";
        private const string LYNDALOGOUTURL = "http://www.lynda.com/ajax/logout.aspx?url=%2fmember.aspx";

        public LyndaParser()
        {
            webClient = new WebClientEx();
            htmlDoc = new HtmlDocument();
            cookieDictionary = new Dictionary<string, string>();
        }


        public string CourseUrl
        {
            get
            {
                return courseUrl;
            }
            set
            {
                var url = value.ToLower();

                if (!url.Contains("lynda.com"))
                    throw new Exception("The url doesn't appear to be a valid lynda url.");

                if (url.StartsWith("https://"))
                    url = url.Replace("https://", "http://");

                if (!url.StartsWith("http://"))
                    url = "http://" + url;

                if (!url.Contains("www.lynda.com"))
                    url = url.Replace("lynda.com", "www.lynda.com");

                courseUrl = url;
            }
        }

        public bool IsLoggedIn
        {
            get;
            private set;
        }

        public bool Login(string userHandle, string password)
        {
            try
            {
                isBusyEvent.WaitOne();
                isBusyEvent.Reset();
                AddStandardHttpHeaders();
                var loginPage = webClient.DownloadString(LYNDALOGINPAGE);

                var cookies = webClient.ResponseHeaders["Set-Cookie"];
                StoreCookies(cookies);

                htmlDoc.LoadHtml(loginPage);

                var loginForm = htmlDoc.DocumentNode.QuerySelectorAll("#frm input").ToList();
                var loginFields = new NameValueCollection();

                foreach (var loginField in loginForm)
                {
                    var attrName = loginField.GetAttributeValue("name", "");
                    var attrValue = loginField.GetAttributeValue("value", "");

                    loginFields.Add(attrName, attrValue);
                }

                loginFields["usernameInput"] = userHandle;
                loginFields["passwordInput"] = password;

                loginFields["log in"] = "log in";
                loginFields["username"] = userHandle;
                loginFields["password"] = password;
                loginFields["remember"] = "false";
                loginFields["stayPut"] = "true";
                //loginFields["resolve"] = "true";
                loginFields["fromUrl"] = LYNDALOGINPAGE;
                loginFields["redirectTo"] = "";
                //loginFields["fromUrl"] = "https://www.lynda.com/login/login.aspx";

                AddStandardHttpHeaders();
                webClient.Headers.Add("Origin", "http://www.lynda.com");
                webClient.Headers.Add("Referer", "http://www.lynda.com/member.aspx");
                webClient.Headers.Add(HttpRequestHeader.Cookie, GetCookieString());

                webClient.UploadValues(LYNDALOGINSUBMITPAGE, loginFields);
                cookies = webClient.ResponseHeaders["Set-Cookie"];
                StoreCookies(cookies);

                if (cookieDictionary.ContainsKey("LyndaAccess") && cookieDictionary.ContainsKey("AspSessionIdCopy"))
                    IsLoggedIn = true;

                if (!IsLoggedIn)
                    throw new Exception("Login failed.");

                return IsLoggedIn;
            }
            finally
            {
                isBusyEvent.Set();
            }
        }

        public Course RetrieveCourse()
        {
            try
            {
                isBusyEvent.WaitOne();
                isBusyEvent.Reset();

                if (!IsLoggedIn)
                    throw new Exception("You are not logged in.");

                if (string.IsNullOrEmpty(CourseUrl))
                    throw new Exception("Course URL is empty.");

                AddStandardHttpHeaders();
                webClient.Headers.Remove(HttpRequestHeader.Cookie);
                webClient.Headers.Add(HttpRequestHeader.Cookie, GetCookieString());
                webClient.RedirectFlag = true;

                var content = webClient.DownloadString(CourseUrl); ;
                htmlDoc.LoadHtml(content);
                var titleNode = htmlDoc.DocumentNode.QuerySelector("h1:first-child");
                var title = titleNode.InnerText.Split('\r')[0];

                var course = new Course();
                course.Title = HttpUtility.HtmlDecode(title).Trim();
                course.Modules = new List<Module>();

                var moduleNodes = htmlDoc.DocumentNode.QuerySelectorAll("#course-toc-outer > li > div > h3 > a").ToList();
                var moduleNo = 0;
                var chapterNo = 0;

                foreach (var moduleNode in moduleNodes)
                {
                    var module = new Module { Title = HttpUtility.HtmlDecode(moduleNode.InnerText).Trim() };
                    module.Chapters = new List<Chapter>();

                    var chapterTitleDomNode = string.Format("#course-toc-inner-{0} > li > dl > dt > b > a", moduleNo);
                    var chapterDurationDomNode = string.Format("#course-toc-inner-{0} > li > dl > dd", moduleNo);

                    var chapterTitleNodes = htmlDoc.DocumentNode.QuerySelectorAll(chapterTitleDomNode).ToList();
                    var chapterDurationNodes = htmlDoc.DocumentNode.QuerySelectorAll(chapterDurationDomNode).ToList();

                    Debug.Assert(chapterTitleNodes.Count == chapterDurationNodes.Count);

                    moduleNo++;
                    chapterNo = 1;
                    foreach (var chapterNode in chapterTitleNodes)
                    {
                        var duration = chapterDurationNodes[chapterNo - 1].InnerText;
                        duration = duration.Trim();

                        var chapter = new Chapter
                        {
                            Title = HttpUtility.HtmlDecode(chapterNode.InnerText).Trim(),
                            FileNameParts = new string[3],
                            TotalMilliSeconds = GetDurationInMilliseconds(duration),
                            Tokens = new Dictionary<string, string>()
                        };

                        chapter.FileNameParts[0] = course.Title;
                        chapter.FileNameParts[1] = moduleNo + "." + module.Title;
                        chapter.FileNameParts[2] = chapterNo + "." + chapter.Title;

                        var vidNo = chapterNode.GetAttributeValue("id", string.Empty);
                        vidNo = vidNo.Replace("lnk-", string.Empty);
                        chapter.Tokens["VideoNo"] = vidNo;
                        chapter.Tokens["Referer"] = chapterNode.GetAttributeValue("href", string.Empty); ;

                        module.Chapters.Add(chapter);
                        chapterNo++;
                    }

                    course.Modules.Add(module);
                }

                if (course.TotalChaptersCount <= 0)
                    throw new Exception("Failed to retrieve course contents.");

                return course;
            }
            catch (WebException we)
            {
                if (we.Message == "Too many automatic redirections were attempted.")
                    throw new Exception("It appears that you are already logged into Lynda.com on other devices. Please log into Lynda.com from a web broswer and logout immediately before attempting to retrieve the course contents.");

                throw we;
            }

            finally
            {
                isBusyEvent.Set();
            }
        }

        public string GetChapterUrl(Dictionary<string, string> tokens)
        {
            try
            {
                isBusyEvent.WaitOne();
                isBusyEvent.Reset();

                if (!IsLoggedIn)
                    throw new Exception("You are not logged in.");

                var chapterUrl = string.Empty;

                AddStandardHttpHeaders();

                webClient.Headers.Remove(HttpRequestHeader.Cookie);
                webClient.Headers.Add(HttpRequestHeader.Cookie, GetCookieString());

                webClient.Headers.Remove(HttpRequestHeader.Accept);
                webClient.Headers.Add(HttpRequestHeader.Accept, "application/json, text/javascript, */*; q=0.01");

                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded; charset=UTF-8");
                webClient.Headers.Add("X-Requested-With", "XMLHttpRequest");
                webClient.Headers.Add("Referer", tokens["Referer"]);

                var urlsFile = webClient.DownloadString(string.Format(LYNDAVIEWCLIPURL, tokens["VideoNo"]));

                webClient.Headers.Remove(HttpRequestHeader.Accept);
                webClient.Headers.Remove(HttpRequestHeader.ContentType);
                webClient.Headers.Remove("X-Requested-With");
                webClient.Headers.Remove("Referer");

                var urlsDoc = JsonConvert.DeserializeObject<ChapterUrlsJsonDoc>(urlsFile);

                foreach (var format in urlsDoc.Formats)
                {
                    if (format.Extension == "mp4" &&
                        (format.Height == "540" || format.Height == "600" || format.Height == "660") &&
                        (format.Resolution == "540" || format.Resolution == "720") &&
                        (format.Width == "960" || format.Width == "880"))
                    {
                        chapterUrl = format.Url;
                        break;
                    }
                }

                return chapterUrl;
            }
            finally
            {
                isBusyEvent.Set();
            }
        }

        public void Logout()
        {
            try
            {
                if (!IsLoggedIn)
                    throw new Exception("You are not logged in.");

                AddStandardHttpHeaders();
                webClient.RedirectFlag = true;
                webClient.Headers.Remove(HttpRequestHeader.Cookie);
                webClient.Headers.Remove("Origin");
                webClient.Headers.Add(HttpRequestHeader.Cookie, GetCookieString());
                webClient.Headers.Add("Referer", "http://www.lynda.com/member.aspx");

                webClient.DownloadString(LYNDALOGOUTURL);
            }
            catch
            {
            }
            finally
            {
            }
        }

        private void StoreCookies(string cookie)
        {
            Debug.Assert(webClient != null);

            if (cookie == null)
                return;

            var value = string.Empty;
            Regex regex = new Regex("ARPT=.+?;");
            value = regex.Match(cookie).Value;

            if (!string.IsNullOrEmpty(value))
                cookieDictionary["ARPT"] = value;

            regex = new Regex("token=.+?;");
            value = regex.Match(cookie).Value;

            if (!string.IsNullOrEmpty(value))
                cookieDictionary["token"] = value;

            regex = new Regex("player_settings_0_1=.+?;");
            value = regex.Match(cookie).Value;

            if (!string.IsNullOrEmpty(value))
                cookieDictionary["player_settings_0_1"] = value;

            regex = new Regex("player_settings_2794346_1=.+?;");
            value = regex.Match(cookie).Value;

            if (!string.IsNullOrEmpty(value))
                cookieDictionary["player_settings_2794346_1"] = value;

            regex = new Regex("AspSessionIdCopy=.+?;");
            value = regex.Match(cookie).Value;

            if (!string.IsNullOrEmpty(value))
                cookieDictionary["AspSessionIdCopy"] = value;

            regex = new Regex("LyndaAccess=.+?;");
            value = regex.Match(cookie).Value;

            if (!string.IsNullOrEmpty(value))
                cookieDictionary["LyndaAccess"] = value;
        }

        private string GetCookieString()
        {
            var cookie = string.Empty;

            foreach (KeyValuePair<string, string> kvp in cookieDictionary)
                cookie += kvp.Value;

            return cookie;
        }

        private void AddStandardHttpHeaders()
        {
            webClient.Headers.Remove(HttpRequestHeader.UserAgent);
            webClient.Headers.Remove(HttpRequestHeader.Accept);
            webClient.Headers.Remove(HttpRequestHeader.AcceptLanguage);

            webClient.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
            webClient.Headers.Add(HttpRequestHeader.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            webClient.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US,en;q=0.8");
        }

        private int GetDurationInMilliseconds(string duration)
        {
            var durationParts = duration.Split(' ');
            var milliSecondsDuration = 0;
            var multiplier = 1;

            for (var i = durationParts.Length - 1; i >= 0; i--)
            {
                var durationPart = durationParts[i].TrimEnd('m', 's', 'h');
                milliSecondsDuration += int.Parse(durationPart) * multiplier;
                multiplier *= 60;
            }

            return milliSecondsDuration * 1000;
        }

        class ChapterUrlsJsonDoc
        {
            public class Format
            {
                public string Extension { get; set; }
                public string FileSize { get; set; }
                public string Height { get; set; }
                public string Resolution { get; set; }
                public string Url { get; set; }
                public string Width { get; set; }
            }

            public List<Format> Formats { get; set; }
        }


        public void LoadCookie(string cookie)
        {
            throw new NotImplementedException();
        }
    }
}
