﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Data.SqlClient;

namespace CourseDownloader.PSWebsite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect(Url.Content("~/Index.html"));
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public JsonResult VerifyLicense(string psUserHandle, string activationId)
        {
            var status = false;

            try
            {
                ValidateString(psUserHandle);
                ValidateString(activationId);

                var connString = "Data Source=psddb123.db.11317694.hostedresource.com; Initial Catalog=psddb123; User ID=psddb123; Password=Noknows@1980;";
                var cmdText = "select activation_id from tb_lic where ps_uid = @psUserHandle and activation_id = @activationId;";

                using (SqlConnection con = new SqlConnection(connString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(cmdText, con))
                    {

                        SqlParameter param = new SqlParameter("@psUserHandle", psUserHandle);
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@activationId", activationId);
                        cmd.Parameters.Add(param);

                        var activationIdFromDb = cmd.ExecuteScalar();

                        if (activationIdFromDb != null && activationIdFromDb.ToString() == activationId)
                            status = true;
                    }
                }
            }
            catch
            {
            }

            return Json(status, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ContentResult GenerateActivationCode(string psUserHandle, string transactionId, string recaptcha_challenge_field, string recaptcha_response_field)
        {
            var status = string.Empty;

            try
            {
                ValidateString(psUserHandle);
                ValidateString(transactionId);

                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("privatekey", "6LeZuOMSAAAAAORYbjMuA5KDVShY3Ep8nmjoMzCv");
                nvc.Add("remoteip", Request.UserHostAddress);
                nvc.Add("challenge", recaptcha_challenge_field);
                nvc.Add("response", recaptcha_response_field);
                byte[] responseBytes = wc.UploadValues("http://www.google.com/recaptcha/api/verify", nvc);
                var captchaResponse = Encoding.UTF8.GetString(responseBytes).ToLower();

                if (captchaResponse.StartsWith("false"))
                    throw new ApplicationException("<p>Invalid captcha code. Please go to the previous page, fill the form correctly and re-submit the form.</p>");

                var activationId = Guid.NewGuid().ToString();
                var connString = "Data Source=psddb123.db.11317694.hostedresource.com; Initial Catalog=psddb123; User ID=psddb123; Password=Noknows@1980;";
                var cmdText = "insert into tb_lic values (@psUserHandle, 'XXXXX', 'XXXXX@example.com', @activationId, @transactionId, @userCreateDate, @licPurchasedDate, 20);";

                using (SqlConnection con = new SqlConnection(connString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(cmdText, con))
                    {

                        SqlParameter param = new SqlParameter("@psUserHandle", psUserHandle);
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@transactionId", transactionId);
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@activationId", activationId);
                        cmd.Parameters.Add(param);

                        var date = DateTime.Today.ToString("u");

                        param = new SqlParameter("@userCreateDate", date);
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@licPurchasedDate", date);
                        cmd.Parameters.Add(param);

                        if (cmd.ExecuteNonQuery() >= 1)
                        {
                            status = "<p>Your activation code is: <b>" + activationId + "</b></p>";
                            status += "<p><b>Pluralsight may BLOCK your account if you download too many videos.</b> Some of our users have experienced this before. Use the tool judiciously and do not overload Pluralsight servers by downloading too many videos/courses in a short span of time. Our recommendation is to download not more than 10 videos, not courses, per day. Enjoy! </p>";
                        }
                        else
                        {
                            throw new ApplicationException("<p>Error occurred. Contact us at pluralsightdownloader@gmail.com</p>");
                        }
                    }
                }
            }
            catch (ApplicationException aex)
            {
                status = aex.Message;
            }
            catch
            {
                status = "<p>Error occurred. Contact us at pluralsightdownloader@gmail.com</p>";
            }

            return Content(status, "text/html");
        }

        private bool ValidateString(string stringToValidate)
        {
            stringToValidate = stringToValidate.ToLower();

            if (string.IsNullOrEmpty(stringToValidate))
                throw new ArgumentException("string can not be empty");

            if (stringToValidate.Length > 50)
                throw new ArgumentException("string length exceeds 50 characters.");

            if (stringToValidate.Contains(' ') || stringToValidate.Contains(';') || stringToValidate.Contains('='))
                throw new ArgumentException("Invalid characters found in the string.");

            if (stringToValidate.Contains("select") || stringToValidate.Contains("insert") ||
                stringToValidate.Contains("update") || stringToValidate.Contains("delete"))
                throw new ArgumentException("Invalid characters found in the string.");

            return true;
        }
    }
}
