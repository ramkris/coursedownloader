USE [psddb123]
GO

/****** Object:  Table [dbo].[tb_lic]    Script Date: 11/06/2013 20:41:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tb_lic](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ps_uid] [varchar](50) NOT NULL,
	[user_name] [varchar](50) NULL,
	[user_email] [varchar](50) NULL,
	[activation_id] [varchar](50) NULL,
	[invoice_id] [varchar](50) NULL,
	[user_create_date] [date] NULL,
	[lic_purchased_date] [date] NULL,
	[amt_paid] [float] NULL,
 CONSTRAINT [PK_tb_lic] PRIMARY KEY CLUSTERED 
(
	[ps_uid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

